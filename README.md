# Documentation
Documentation pour la réalisation des prototypes du Hackathon - Capter Pasteur

## Index
[CouchDB](#couchdb)

[Arduino Ethernet Shield v2](#arduino-ethernet-shield-v2)

[Arduino & CouchDB](#arduino-couchdb)
## CouchDB
### Introduction
CouchDB est une base de donnée se basant sur des documents et présentant une API HTTP REST.

Référence de l'API : https://docs.couchdb.org/en/stable/api/index.html

Le serveur local de pasteur héberge une instance de CouchDB à l'adresse suivante : `https://data.hotelpasteur.fr/`.

L'interface d'administration est accessible à l'adresse suivante : [https://data.hotelpasteur.fr/_utils/](https://data.hotelpasteur.fr/_utils/).

Cette dernière n'authorise pas la visualisation de toutes les base de données pour les utilisateurs "normaux".

Une fois authentifié, il est possible de visualiser sa propre base de donnée à l'adresse suivante : [https://data.hotelpasteur.fr/_utils/#database/chambre-236/_all_docs](https://data.hotelpasteur.fr/_utils/#database/chambre-236/_all_docs) (pour la base de données `chambre-236` authentifié avec l'utilisateur `equipe-rouge`).

Les documents se présentent sous la forme d'un JavaScript Object (JSON) contenant au minimum une clée d'identifiant (`_id`) et une clée de révision (`_rev`)
##### Exemple d'un document CouchDB avec une donnée et un horodatage
```json
{
  "_id": "755c32a5c5aa94519e59fc694902d816",
  "_rev": "1-827c9725ad0d9fcfa437fade233c292c",
  "datapoint": 135,
  "created": "2021-03-07T08:16:47Z"
}
```
##### Exemple d'un document CouchDB avec un tableau de données
```json
{
  "_id": "755c32a5c5aa94519e59fc694902d816",
  "_rev": "1-827c9725ad0d9fcfa437fade233c292c",
  "datapoints": [
      {
            "datapoint": 15.6,
            "created": "2021-03-06T12:00:00Z"
      },
      {
            "datapoint": 12.5,
            "created": "2021-03-07T12:00:00Z"
      },
      {
            "datapoint": 16.3,
            "created": "2021-03-08T12:00:00Z"
      }
  ]
}
```
### Créer un nouveau document avec Basic Auth
https://docs.couchdb.org/en/stable/api/database/common.html#post--db

https://docs.couchdb.org/en/stable/api/server/authn.html#basic-authentication

Pour une base de donnée "db"
##### Requète :
```http
POST /db HTTP/1.1
Accept: application/json
Authorization: Basic dXNlcm5hbWU6cGFzc3dvcmQ=
Content-Type: application/json

{
    "servings": 4,
    "subtitle": "Delicious with fresh bread",
    "title": "Fish Stew"
}
```
Où `dXNlcm5hbWU6cGFzc3dvcmQ=` est le codage en base64 de `username:password`

##### Réponse :
```http
HTTP/1.1 201 Created
Cache-Control: must-revalidate
Content-Length: 95
Content-Type: application/json
Date: Tue, 13 Aug 2013 15:19:25 GMT
Location: http://localhost:5984/db/ab39fe0993049b84cfa81acd6ebad09d
Server: CouchDB (Erlang/OTP)

{
    "id": "ab39fe0993049b84cfa81acd6ebad09d",
    "ok": true,
    "rev": "1-9c65296036141e575d32ba9c034dd3ee"
}
```
## Arduino Ethernet Shield v2
### Documentation officielle
Documentation de l'Arduino Ethernet Shield v2 : https://www.arduino.cc/en/Guide/ArduinoEthernetShield

Tutoriel pour les requètes HTTP avec l'Ethernet Shield v2 : https://arduinogetstarted.com/tutorials/arduino-http-request
## Arduino & CouchDB

Un [exemple](/arduino_http_01.ino) de code transmettant un objet JSON à la base de donnée CouchDB est donnée dans le repertoire.

http://blog.bitheap.net/2011/01/adding-data-to-couchdb-from-arduino.html

https://github.com/prasmussen/SensorBox



