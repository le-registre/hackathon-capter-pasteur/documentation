/*
  Based  on Web client example by David A. Mellis

 This sketch connects to a CouchDB database (http://data.hotelpasteur.fr:5984)
 and send an json document with a key "message" and a value "hello-world" 
 using an Arduino Ethernet Shield 2 and ArduinoJson library.

 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13


 */

#include <Ethernet.h>
#include <SPI.h>

#include <ArduinoJson.h>
#include <ArduinoJson.hpp>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
// IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)
char server[] = "data.hotelpasteur.fr";
int port = 5984;
// Set the static IP address to use if the DHCP fails to assign
IPAddress ip(192, 168, 0, 177);
IPAddress myDns(192, 168, 0, 1);

String auth = "ZXF1aXBlLW1hdXZlOkM0ODN1NUs1WWo0alFqbko=";

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

unsigned long byteCount = 0;
bool printWebData = true; // set to false for better speed measurement

// We create the Json object to reserve the memory space on the Arduino (change size according to the number of keys)
const int capacity = JSON_OBJECT_SIZE(1);
StaticJsonDocument<capacity> doc;
String data = String("");

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  doc["message"] = "hello world!";
  serializeJson(doc, data);
  // start the Ethernet connection:
  Serial.println("Initialize Ethernet with DHCP:");
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
      while (true) {
        delay(1); // do nothing, no point running without Ethernet hardware
      }
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
  } else {
    Serial.print("  DHCP assigned IP ");
    Serial.println(Ethernet.localIP());
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.print("connecting to ");
  Serial.print(server);
  Serial.println("...");

  // if you get a connection, report back via serial:
  if (client.connect(server, port)) {
    Serial.print("connected to ");
    Serial.println(client.remoteIP());
    // Make a HTTP request:
    client.println("POST /petite-loge HTTP/1.1");
    client.println("Host: data.hotelpasteur.fr");
    client.println("Connection: close");
    client.println("Accept: application/json");
    client.println("Authorization: Basic "+ auth);
    client.println("Content-Type: application/json");
    client.print("Content-Length: ");
    client.println(data.length());
    client.println();
    client.println(data);
  Serial.println("data : " + data);
  } else {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop() {
  // if there are incoming bytes available
  // from the server, read them and print them:
  int len = client.available();
  if (len > 0) {
    byte buffer[80];
    if (len > 80) len = 80;
    client.read(buffer, len);
    if (printWebData) {
      Serial.write(buffer, len); // show in the serial monitor (slows some boards)
    }
    byteCount = byteCount + len;
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println("disconnecting.");
    client.stop();
    // do nothing forevermore:
    while (true) {
      delay(1);
    }
  }
}

